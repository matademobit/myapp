import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styles: []
})
export class BodyComponent implements OnInit {

  mostrar = true;

  frase: any = {
    autor: 'Ben Parker',
    mensaje: 'Un gan poder requiere una gran responsabilidad'
  } 

   personajes : string[] = ["Spider Man", "SuperMan", "Batman"];



  constructor() { }

  ngOnInit() {
  }

}
